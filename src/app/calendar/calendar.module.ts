import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar.component';

import { PrimengModule } from './primeng/primeng.module';

@NgModule({
  imports: [
    CommonModule,
    PrimengModule
  ],
  exports: [ 
    CalendarComponent 
  ],
  declarations: [
    CalendarComponent
  ]
})
export class CalendarModule { }
