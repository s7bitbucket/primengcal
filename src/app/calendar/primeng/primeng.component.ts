import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

@Component({
  selector: 'cal-primeng',
  templateUrl: './primeng.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './primeng.component.css'
  ]
})
export class PrimengComponent implements OnInit {
  @Input() value: Date;

  constructor() {
  }

  ngOnInit() {
    if (this.value != undefined)
      this.value = new Date(this.value);
    else
     this.value = new Date();
  }

}
