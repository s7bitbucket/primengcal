import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrimengComponent } from './primeng.component';

import { CalendarModule as PrimeCalModule } from 'primeng/calendar';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    PrimeCalModule
  ],
  exports: [PrimengComponent],
  declarations: [PrimengComponent]
})
export class PrimengModule { }
